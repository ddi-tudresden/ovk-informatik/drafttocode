# draftToCode

Webprogramm zur Erstellung von Struktogrammen / PAP sowie zur Umwandlung dessen in Code (Python, PHP, Javascript)

[Datenfluss-Diagramm](https://drive.google.com/file/d/1BqLCW7vIB3DXViDjjPmEk6r4qjEnT8jv/view?usp=sharing)


# for deployment

- 'sed', 'sassc' need to be installed
- run deploy.sh in shell to generate CSS file
- run npx webpack to generate JS file


# for development

- 'inotify-tools' need to be installed
- run develop.sh to automatically build CSS and JS files on saving